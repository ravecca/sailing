# Resources

Interesting things about sailing.

## Racing rules tool

I made this web tool for rules exploration. Move and rotate the boats and the rules applied are shown.
Only rules 10, 11, 12, 13, 15, 16, 17 (Part 2 > When boats meet > Section A * Right of way AND Section B * General limitations)

[Racing Rules tool](https://gd.games/ravecca)

## Rules

There are a lot of information ready for those willing to learn more about rules.
The main book of course is the Sailing Rules itself, but there are two very interesting books:
* The Case Book, where interpretations are stated and is shown how to analyze each case.
* Call Book for Team Racing.

[World Sailing Rules and regulations](https://www.sailing.org/inside-world-sailing/rules-regulations/racing-rules-of-sailing/)

## Wind shifts game

The intention here is to approach to explain the wind shifts through experience in a game-like interface.
Works on mobile and desktop browsers. On desktop it can be paused, and the wind can be rotated with arrow keys.

[Wind shifts Game](https://ravecca.itch.io/borneos)

## Optibabel game

Optibabel is a puzzle game for little ones. Put it together and learn optimist parts names.

[Racing Rules tool](https://gd.games/ravecca)