# Debriefs....

##2023

- [Monterey!](./monterey23.md)

##2022

- [Golden Bear](./tigoldenbear.md)
- [C420 Midwindter](./c420midwinters.md)
- [Treasure Island Norcal](./tinorcal.md)
---
- [Optimist training videos](./optimist.md)
- [420 training videos](./420.md)
- [Monterey!](./monterey.md)
- [Santa Cruz Sunday and final debrief](./santacruz-sun.md)
- [Santa Cruz Saturday](./santacruz-sat.md)
- [Santa Cruz - pre trip](./santacruz.md)
- [Norcal Richmond YC - sept 24/25 2022](./norcal.md)
