# 2023 PCISA 4/5 – Golden Bear

[PCISA Golden Bear documents](https://scores.hssailing.org/s23/2023-pcisa-golden-bear/notices/)

[Results Gold](https://scores.hssailing.org/s23/2023-pcisa-golden-bear/)

[Results Silver](https://scores.hssailing.org/s23/2023-pcisa-silver-fleet-golden-bear/)

## Short recap

Things we talked about this weekend: 
- Lightwind cycle. How to adjust our body position and sails trimming according to the wind speed cycle. 
- Focus on layers. Or how to manage our focus on different layers.
- Rule 42 propulsion.

## Saturday - Sara coach notes 

Notes from today (Sara)
1) wind was shifting right consistently for about two hours, they can start to predict shifts and lifts from that, winners of the races were tacking onto the lifts
2) Like you said at the team meeting, we want to eliminate as many mistakes as possible. I saw a lot of mismatched body weight and heeled boats. They should remember to sit together and flatten the boat. They can also do more things to increase their ability, like deeper rolls in light wind.
3)they should use their time on land to see the course from an outside perspective, and plan for their next races. They have the best view of the course and what the fleets are doing, they should use it to their advantage.
4) They should keep an eye on controls especially in shifty weather such as today. The wind went from 1 knot to about 8 knots, and the controls should adjust accordingly. They should check their controls as soon as they get into a new boat.

--- End Sara coach notes ---
## Saturday 

Wind condition. A cloud covered and cold day, no wind on the morning stated a very unstable day. We received wind from 015 (North) to West and we had a big delay to finally wind stablished on West direction (Golden Gate direction). Inside the TI cove there are some geographic obstacles for the wind ending on a very difficult prediction. A small wind direction change outside our area results in a totally different behaviour inside the cove. 

Under these mutating scenario we need to observe very carefully the race course before the start. And maybe is a good practice to delay our decisions to the last point before the start, being ready for a sudden change.

For this light wind day we had key points on how we work the wind speed cycle with the sails and our weight. Please watch following videos:
 
 - 'Body weight & Sheet sequences' video noted on [Basics section](../basics/index.md). 
 
 - 'Acceleration' video noted on [Basics section](../basics/index.md).

## Sunday

Sunday had all the 'difficult day' ingredients. The unstable wind condition, cold weather, heavy gusts, etc stated a situation where building a 'tactic' resulted a bit unreal or difficult to imagine. A way to manage this, is giving a space to that tactic plan in our minds. The excersise to split things on Layers or categories help us to change the focus on each one depending on importance across time. As we discussed before, this focus managed will result in less mistakes and a better vision on our different goals.

There are things we can not change. The uncomfortable state is one of them, specially on a day like Sunday. It is important to know, everyone on the fleet is on the same situation, also those winning the race. The difference can be how each sailor manage that situation. Leaving things we can not change out of the discussion frees us to worry about what we can change, the tactic plan is one thing we can take care, so we move this layer to a top priority and keep others on the lower levels.

## Rule 42 - propulsion

Please read this following paragraph is the basic rule about Propulsion. All actions outside this basic rule are called 'exceptions'. Please read 'Rule 42 interpretations' listed below to understand better the 'exceptions' and how they work in the context.

::: tip 42.1 Basic Rule
Except when permitted in rule 42.3 or 45, a boat shall compete by using only the wind and water to
increase, maintain or decrease her speed. Her crew may adjust the trim of sails and hull, and
perform other acts of seamanship, but shall not otherwise move their bodies to propel the boat.
::: 

[Rule 42 interpretations](https://www.sailing.org/tools/documents/Rule42InterpretationsMay21-[27359].pdf)

We need to keep rules simple to understand better the main rule idea, and after reading this Rule 42 notes it is clear that what we saw on those races are not inside rule 42.

Please watch following video and try to understand the Umpire ideas, he is allways refering back again to the basic rule.

[Finn class rule 42 common breaches](https://www.youtube.com/watch?v=u6OFea9rZA8&ab_channel=TheFinnChannel)