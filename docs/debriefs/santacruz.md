
# Santa Cruz - sept 2022

[Santa Cruz regatta website](https://theclubspot.com/regatta/lPSM8BUWUv) Read the documents.

[Bay area youth sailing](https://www.bayareayouthsailing.org/bays-series)

## Reading the Sailing Instructions

You will find the SI on the regatta website, there are some references about flags, so we can refresh those flags here.

### General flags

![Lima Flag](https://nauticpedia.com/wp-content/uploads/2016/08/Bandera-L-codigo-internacional-se%C3%B1ales.jpg) The L flag Lima, means "Follow me, approach", if it is used on the water you must follow the race commitee, and if it is used on land means some information is displayed on the notice board.

![AP flag](https://www.flagsonline.it/uploads/2016-6-6/315-205/intelligenza.jpg) The AP flag means postponement. The races are delayed. If it is on the water, when it comes down it means one minute later the starting sequence will begin. On land, when it is taken down it means not earlier than 45 minutes the starting sequence will begin.
### Course definition Flags

![Numeral One](https://nauticpedia.com/wp-content/uploads/2016/08/numero-1-se%C3%B1al-internacional-300x150.png) Numeral One > Note it has only one coloured zone, if you can imagine the white like background. Remember Japan flag is the 1

![Numeral Two](https://nauticpedia.com/wp-content/uploads/2016/08/numero-2-se%C3%B1al-internacional-300x150.png) Numeral Two > Following the same idea, it has two zones by colour.

![Numeral Three](https://nauticpedia.com/wp-content/uploads/2016/08/numero-3-se%C3%B1al-internacional-300x150.png) Numeral Three > Three coloured zones.

![Numeral Four](https://nauticpedia.com/wp-content/uploads/2016/08/numero-4-se%C3%B1al-internacional-300x150.png) Numeral Four > It has 4 red coloured zones

### Starting line flags

![Oscar Flag](https://nauticpedia.com/wp-content/uploads/2016/08/Bandera-O-codigo-internacional-se%C3%B1ales.jpg) Optimist will have the Oscar flag, maybe they use the Optimist simbol or this Oscar flag. 

![Foxtrot Flag](https://nauticpedia.com/wp-content/uploads/2016/08/Bandera-F-codigo-internacional-se%C3%B1ales.jpg) FJ will use Foxtrot flag , the red diamond 

420 will use class sign flag.

![First repeater](https://www.banderasphonline.com/wp-content/uploads/2020/08/comprar-bandera-GALLARDETE-PRIMER-REPETIDOR-para-barco-n%C2%BA12345-codigo-internacional-de-se%C3%B1ales-nato-220x165.png) General Recall (First repeater), it means unidentified boats are on the course side at the start, the race will be restarted. 

![X Flag](http://nauticpedia.com/wp-content/uploads/2016/08/Bandera-X-codigo-internacional-se%C3%B1ales.jpg) X Flag. Raised seconds after a start, this means one or more boats are over early. They must come back to the prestart side and start again. Note if you have One minute rule on your start you must come back rounding the ends of the starting line. When all over early boats restart, the flag is taken down, or 4 minutes after the start

### Starting line penalties flags

![I flag](http://nauticpedia.com/wp-content/uploads/2016/08/Bandera-I-codigo-internacional-se%C3%B1ales.jpg) The I flag rule, means at a starting line, boats over the line early on the last minute, must come back to the prestart side rounding the starting line ends. It means you can not cross the line coming from the course side (windward), to the pre start side (leeward), to start correctly, you need to go around the starting line marks.

![U flag](http://nauticpedia.com/wp-content/uploads/2016/08/Bandera-U-codigo-internacional-se%C3%B1ales.jpg)The U flag means, people over early on the last minute are DSQ disqualified. BUT if the race is restarted, after of a general recall for example, those boats can start again.

BLACK FLAG. It is a full black flag. It means the same like U flag, people over early on the last minute is disqualified, but you CAN NOT start again if the race is restarted.

### Penalty system

We will be using one turn penalty all classes.

## Notes before getting there 

Please be aware about all safety needs according to your class rules.

::: warning Safety
Lifejackets, whistles, tow lines, baylers...
:::

### 420s
C.3.6 A whistle shall be attached by a lanyard to the PFD. The lanyard shall be long enough to permit the sailor to use the whistle while wearing the PFD.  
C.4.4 Each boat shall carry one line designated as a towing line. This line shall be at least 40 ft (12 m) long, 3/8 inch (9 mm) diameter, shall float.
C.4.5 Each boat shall use a line attached from the rudder to the boat that will keep the rudder connected to the boat in the event of capsize or turtle.

[Club 420 class rules](https://club420.org/assets/documents/other_docs/c420-Class-Rules-02-03-2022.pdf_.pdf?assets/documents/other_docs/c420-Class-Rules-07-2019.pdf)

### Optimist
4.2 (a) The helmsman shall wear a personal flotation device to the minimum standard ISO 12402-5 (Level 50) or equivalent. All fastening devices supplied by the manufacturer shall be used in the manner intended. A whistle shall be carried securely attached to the personal flotation device.

4.3 The following equipment shall be on board while racing:
(a) One or more bailers which shall each be individually and securely attached to the hull by a lanyard(s) or elastic cord(s). One bailer shall have a minimum capacity of one litre.
(b) A painter of a single piece of buoyant rope, not less than 5 mm diameter and not less than 8 m long securely fastened to the mast thwart or mast step. (see also 3.2.6.1).
(c) A paddle of wood and/or plastic, weighing not less than 200g, having corner radii of minimum 5 mm and a blade able to contain a rectangle of 200 mm x 130 mm shall be securely attached to the hull by a lanyard or elastic cord.

[Optimist International Class Rules](https://www.optiworld.org/uploaded_files/2021%20IODA-Rules%20-%20Approved%20WS%20-%20Jan%208_21.pdf_2815_en.pdf)


## Sailing upwind in waves

There is the problem about waves stopping the boat, hitting a wave is draining boat power and the result is a loose of speed. But also there is another important issue, and this is the apparent wind.
When we go up the wave, it's pushing us leeward, and the apparent wind decreases intensity and moves to a little lift. On the way down, when we go down the wave, it is pulling us to windward so the apparent wind increases intensity and shift like a header. This is the explanation why we do this technique:
- Head up climbing the wave.
- Head down, when descending the wave.

[Sailing upwind in waves](https://www.youtube.com/watch?v=Nf4s5CrAp04)
