
# Santa Cruz - Saturday October 1st 2022

[Santa Cruz regatta website](https://theclubspot.com/regatta/lPSM8BUWUv)
[Bay area youth sailing](https://www.bayareayouthsailing.org/bays-series)

## Saturday wind

The day started with no wind at all, and then started a South West SW breeze, then later on the day turning right slowly and with some pressure. In general it was more presure on the left, outside the bay, there was a sailboat sailing fast out there on one 420 start, maybe 2nd race, so it was clear at that point was more wind there. Things change fast and we must be with our sight on windward far away trying to predict what is happening with the race area.

## Saturday current & sweel

When races started, we noticed current pushing to West W. Light but noticeable. Swell was building up during the day finishing with an important height.
Upwind with no wind usually you use the current to push you forward and pick that tack aligned. With gust you pick the other tack (with current not pushing aligned from the back). But with swell, the things get a bit more complicated because the swell has a direction and it has a lot of influence on the apparent wind. The idea is to try to use the swell to propel the boat building apparent wind when it pulls the boat to windward. We need to test both tacks and try to follow the best swell aligned one at the moment the wind die. It is only a matter to feel the boat on both tacks and decide which is best when the wind die.

The way we move the boat on the sweel needs a lot of focus and concentration, and a small angle change makes a lot of difference, there is no other way than feel it.

## Champ fleet starts

The RC is using the race course numbers not related to the fleets, it means we need to keep one eye on the flag they are flying.
In general we need to be more agressive on the starts (420s and Optimist too), because the result of the start makes a big difference on upwind tactic. And of course, we need to be convinced wich side to go and why, before the start and make a plan according to it.

## Champ fleet upwinds

We want to go to somewhere because we saw a difference, maybe wind pressure, maybe some cloud... and when we get there, we get the advantage, we need to cross the fleet, and look for positioning. Remember you are only ahead of someone when you cross his bow.