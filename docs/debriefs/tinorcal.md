# Treasure Island Norcal - feb 2023

[PCISA website](https://pcisa.hssailing.org)

[PCISA NorCal Notice of Series](https://hssailing.org/schedule_news/docs/pcisa_docs/2022-2023-PCISA-NorCal-NOS.pdf)

[Results link for norcal 5 gold](https://scores.hssailing.org/s23/2023-norcal-gold/full-scores/)

[Results link for norcal 5 silver](https://scores.hssailing.org/s23/2023-norcal-silver/)

## Saturday 11th Debrief

Short notes about what we talked today.

::: tip Big lesson to learn today: 
Be close to the race committee on the pre-start waiting time.
:::

As we stated on the briefing, in these regattas with no visual signs, no flags, we have no clue about what is happening on the pre-start, so we need to be close. We can make an upwind test but always keeping it short and coming back to the starting line soon enough to get the news about RC intention.

Also if we need to fix something or change some settings, or clothes or any time consuming stage, we need to come firts besides the RC or near the starting line.

Note: I am using races number reference on Gold. Silver was one race behind, so race 1 for Silver is just before race 2 Gold. That is why gold had all time a fleet to have a reference about what was happening with the wind.

Today we had three scenarios:

On Races 1 & 2 upwind tactics was easy to read because the right side, the Bay Bridge Side had clearly more steady wind. Also this gust coming from the channel had a good angle, so we had pressure and also angle. These presure difference was also sustained on the downwind, being better to pick the same windy side on that leg too.

Races 3 & 4 had a different scenario. Wind went down a lot, left gusts appear but after a new drop the wind went right a lot. Remember the windward mark being moved to the right on race 4. And this was only a temporary big gust from the right, one of the lasts before wind shifting to the left.

As we expected, at some point the wind will rotate to left, to the Golden Gate, direction we can expect specially near 1pm.

The race 3 had signals about left shifts, but on race 4 just after starting it was clearly a big left persistent shift. One big signal about this was the right side silver fleet boats, close to the right layline, near the Bay Bridge with absolutely no wind. So just after that start we had a zone on the upwind where we never want to go, a no wind zone. This observation narrowed options to middle to left side on the upwind. And briefly after start the pressure on left was increasingly appearing.

When this kind of shift covers the race course, we find "pressure lanes", the lane closer to the shift has more pressure than the leeward one and so on, and these lanes are very narrow, but the pressure on the windward one is steady not covering or moving to the leeward one.

That is why, even if the port tack on the upwind was straight to the windward or finish line, it was necessary to keep investing time on starboard tack to get into the windward lane. How far, it depends on how much wind we have and what happens with other boats refering on position / tactic.

Then, wind died and got more unstable, for races 5 & 6. On these two races there where a lot of mistakes on the starting line, being on a place far from the line with no wind, just waiting. On this no-wind case, and also unstable, we need to position our boat on the middle of the starting line and wait for the 3 minutes there, so we can watch better and perceive what is happening with the wind, and we can be sure we can move to the place we consider better fast enough.

The two last where very short races, where the start has a big weight or importance, if we fail on the start it is very difficult to be on the first places, there is no time for recovering and distance between boats starting first and those not doing a good start becomes big due to the unstable conditions.

## Sunday 12th Debrief

The day started with no clouds, cold and sunny. No wind at all. Therefore we where expecting the wind coming from the Golden Gate around noon. After FJs where drop, the breeze started from that direction.

First race, was clearly favored on the right with more angle and pressure. Right side on this direction was the TI club side. For Gold fleet it was easy to read that because Silver fleet, who started first this time, was on the first upwind and people on the right was clear doing very well and, at the same time, some of the last boats being on the left had no wind at all.

On the second race, the wind went back a bit to the original position and decreced, making the upwind more difficult to read, and it was challenging to know where to build the tactics. But again, the right had predominance, in general.

Next races were more difficult because the upwind balanced more and more and the moments one side favored clearly were shorter.

Then, at the debrief we talked about the idea of these balanced races, or this ambiguous scenario. The races where is not a clear side to go, not a simple idea to stick to, and decisions became more and more complicated.

Also ours own mistakes are more difficult to read, because also the root of the mistake is uncertain. On the other hand, on a 'side favoured' race, the mistake is usually sticked to picking the wrong side, but here is far more complicated.

The idea is more on the side of 'opportunity window'. A short period of time, on the race, where we can take advantage. At the moment we identify we are on that situation, we need to risk more, and after the gain is on our pocket again we race on a conservative way.

The hardest part is to be able to apply two different kind of tactics: high risk vs conservative. If we can mix them well, and manage how to apply the focus on either one, we have more chances to win. 

Usually one sailor can be defined on one way or the other. You have the 'mathematical' tactic sailor, can be related to fell better on a conservative way of movements. And the other, extreme risky sailor, who feel better on a more diverse race course, with 'best sides'. Whell this 'ambiguous' scenario receipt is a mix of both personalities in one boat.
