
# Monterey - oct 2022

[Monterey regatta website](https://pcisa.hssailing.org/schedule-results/details/2022-sea-otter-pcisa-1-5)

## Wind

Both days were very light, very patchy and unstable. 

Saturday started with wind coming from the pier, and later rotating to left gradually during the day, until extreme moment where coming back to the beach was done on a beat to windward. Then the last part of the day it came back to right with some puffs.

Sunday was more to the right at the beginning of the racing time, and then again rotating to the left but not as far as the day before.

This rotation in a long term, keeps the 'new wind' coming from the new direction, more to land, fighting with the 'old wind' from the old direction, more to the sea. In some spots of the racecourse one was gaining over the other. I remember last upwind on Sunday was in majority on the right better, maybe because wind intensity.

And how can we predict or at least understand this wind behavior? How can we 'read' the race area? 
First, we need to be open to perceive, don't overthink and try to use the [observation around and in different distances](../basics/#three-distance-circles).

On two persons boats, you can split the job and have a better focus, for example, going upwind the crew can be in charge of observations/tactics, and skipper on speed; and on downwind the opposite.

### Wind schemes

Sunday we talked about wind schemes, and how important is to race according to the scheme we are situated on the specific day.

We can not race the same on an **oscilating** shifts day, and on a **persistent** shift day. The first matches very well with a 'mathematic' and logic approach, and the second with a more 'instinctive' and risky way.

So on a **persistent** shift day, and also patchy and sometimes a bit crazy day we need to switch our tactic approach to solve better the problems we found. It should be everything more risky, decisions made more based on 'feelings' than 'analysis' and not overthinking, just acting.

We need to observe the sides of the fleet, the boats on the fleet boundaries and specially those on the back. Why? because the shifts are big and steady and they reveal on those positions. On a normal day this doesn't happen and is enough for us to watch windward on a small angle area.

## Current

Both days looked like current aligned to the wind. I was not there so is difficult to know but how the fleet was struggling to fetch the pin side of the starting line it shows that direction... Saturday the race course was nearest to the beach so the influence was less than Sunday, when they set the race course far outside, and it should be deeper, and therefore with more current.

If you observe current remember to add it to your speed calculation and test very well how many seconds you need to fetch the line from a certain point (can be the RC Boat), on very light conditions this estimation can be very hard to do.

Anyway, we can have a general idea about how the current flows, but we need a very precise information about angle and speed on every mark and this can only be done at each mark on an specific moment. Is good to check this every time we round a mark and specially when we arrive to the starting line before each start.

## Boat speed

In general we can say, the boat speed and consistency on these short courses and light wind, was a high importance subject. Please check the technique described on this video. [Body weight & Sheet sequences](https://www.youtube.com/watch?v=JlN-HzcwM1I)

A fast boat and in 'automatic mode' is crucial to be able to use our focus and concentration on tactics. Is similar to playing an music instrument, when you watch a musician with absolute technique mastering, then he can develop the interpretation of the music, without thinking on the instrument.

## Rules

When the light wind reigns, the rocking appears... and others 'out of the box' techniques.

It's a difficult decision when everyone is doing this... what to do? What worked for me is to think when you do that, you are on other sport, not sailing... So is our decision to keep sailing or not. And remember that sport is not about results, it is more about challenges, and this can be a big one.

::: warning Breaking rule 42?
Then: that sailor is on other sport
:::

**BUT**, of course we have some classes in sailing where rule 42 is limited, check this out...

[Finn free pumping](https://www.youtube.com/watch?v=hEQttj8E1b0)

[470 with Oscar flag => free pumping only on certain conditions](https://youtu.be/cot9Gej4Ag8?t=355)

[RS:X => when to pump, is part of strategy because energy management](https://youtu.be/j1UOUq6Gn_E?t=8)

::: details Rule 42 Propulsion

42 PROPULSION

42.1 Basic Rule
Except when permitted in rule 42.3 or 45, a boat shall compete by
using only the wind and water to increase, maintain or decrease her
speed. Her crew may adjust the trim of sails and hull, and perform
other acts of seamanship, but shall not otherwise move their bodies to
propel the boat.

42.2
Prohibited Actions
Without limiting the application of rule 42.1, these actions are
prohibited:

42.3

(a) **pumping**: repeated fanning of any sail either by pulling in and
releasing the sail or by vertical or athwartship body movement;

(b) **rocking**: repeated rolling of the boat, induced by
 - (1)body movement,
 - (2)repeated adjustment of the sails or centreboard, or
 - (3)steering;

(c) **ooching**: sudden forward body movement, stopped abruptly;

(d) **sculling**: repeated movement of the helm that is either forceful
or that propels the boat forward or prevents her from moving
astern;

(e)repeated tacks or gybes unrelated to changes in the wind or to
tactical considerations.

**Exceptions**

(a) A boat may be rolled to facilitate steering.

(b) A boat’s crew may move their bodies to exaggerate the rolling
that facilitates steering the boat through a tack or a gybe,
provided that, just after the tack or gybe is completed, the boat’s
speed is not greater than it would have been in the absence of the
tack or gybe.

(c) When surfing (rapidly accelerating down the front of a wave),
planing or foiling is possible
 - (1) to initiate surfing or planing, each sail may be pulled in
only once for each wave or gust of wind, or
 - (2) to initiate foiling, each sail may be pulled in any number
of times.

(d) When a boat is above a close-hauled course and either stationary
or moving slowly, she may scull to turn to a close-hauled course.

(e) If a batten is inverted, the boat’s crew may pump the sail until
the batten is no longer inverted. This action is not permitted if it
clearly propels the boat.

(f) A boat may reduce speed by repeatedly moving her helm.

(g) Any means of propulsion may be used to help a person or
another vessel in danger.

(h) To get clear after grounding or colliding with a vessel or object,
a boat may use force applied by her crew or the crew of the other
vessel and any equipment other than a propulsion engine.
However, the use of an engine may be permitted by rule 42.3(i).

(i) Sailing instructions may, in stated circumstances, permit
propulsion using an engine or any other method, provided the
boat does not gain a significant advantage in the race.

:::

[Rule 42 interpretations World Sailing](https://www.sailing.org/tools/documents/Rule42InterpretationsMay21-[27359].pdf)

## Points & drops

In other regattas usually you find a drop. The total points are the sum of each race excluding one after a certain number of races. Some time a go, the excluded numbers of races were 2, and then later, World Sailing recomendation was only 1, and there is a big chance for future to have no discards at all.

So, this regatta format, with no discard, is aligned with the mentioned possibility of change on general sailing sport.. in some future day.

The absence of discards collide when something extraordinary happens, for example equipment failure / breaks, disqualifications, etc. But other than that, it can be said is as fair as one discard system, and it rewards the consistency on places.