
# Santa Cruz - Sunday October 2nd 2022

[Santa Cruz regatta website](https://theclubspot.com/regatta/lPSM8BUWUv)
[Bay area youth sailing](https://www.bayareayouthsailing.org/bays-series)

::: warning Congratulations!
Congrats to all sailors and parents! :boat: :rocket:
:::

![EYC Team!](/sailing/eyc-team.jpg)

## Speed & lane

We need two things to be able to build a good strategy plan: speed and lane. Those are the first things sailors focuses when they move to a new class, also very experienced sailors... and why? because it is the first step to have our boat on 'automatic mode' with at least the same speed and the same lane angle than others in the fleet. This allows the sailors to take the 'head out of the boat', and focus on perceive wind patterns, work on the [three distance circles](../basics/) and elaborate the strategy.

### Speed

Every boat would be on a little different speed range, depending on team experience, weight, skills on different conditions, settings, etc. There are too many factors and that is why is so difficult to have **always** good speed. The basic work is done training and studying the specific boat, but we can say the speed is in general fluctuant for everyone.

The important thing is to know how this affect our strategy and plan according to that. If we are a slow boat for the day, we should not react to that condition tacking when we feel uncomfortable sailing wiht people around, because that ends on a 'reaction' tactic, bouncing within other boats actions and not with our own plan. Usually when this happen we find ourselves on a completely different spot on the upwind than we planned to be.

Finally the speed difference maybe is not a lot, and if racing is made in a straigt line, is only a small distance as a result. But because it affects tactics, it ends on a big distance gap.
### Lane

After the start is crucial to be able to keep our lane **paralell** to the leeward boat's lane. To do that, we need to fell comfortable pointing higher and at the same time have good speed. Ideally we can have speed and go up at the same time but, this is not the frequent scenario. The skill needed to be practiced is the ability to sail upwind, a little up than close hauled and at the same time keep our speed close to maximum, and on the other hand, to sail a little down and accelerate over maximum. This gives us a **laterall** movement, being high value for upwind tactic.

On boats with big difference on speed with little angle differences in some conditions (windsurfers, 470s, lasers) this is a major subject on tactics. They can sail heading down starting planing and maybe 1/3 faster, but very low. This is used on tactic positioning.

## Starts

We talked about how a start weight too much on importance on smaller courses. It's not the same to have a 30 minutes upwind where the speed can be developed gaining difference with others (because it depends on time), and the distance to use on the tactics is huge giving same gain on shifts or field differences, than the kind of short race courses we have.

So, in generall the weight of the start is too big on smaller courses, so we need to have a good start. The gain is big and therefore we need to take a risk trying to make the better start. I think this was proved on Sunday's start where the starts looked more on this way.

## Upwind

The main subject on this venue was the swell. It's cycle affects the boat's movement and therefore the apparent wind. The feel for incresing - decreasing wind cycle is bigger with light wind, because proportion with real wind is also bigger. In general when we feel the presure, the boat should have enought leech tension, but at the same time leech should be open (both), the weight should be back and the boat should flatten at the same time it accelerates. Usually we move our shoulders back to the stern and outside to have this done, but the crew can also have a very active movement on hanging with hand on the trapeeze.

And also it was the light wind, and the big tactic goal to catch pressure and chain pressure patches on the upwind.

But the important think is allways, with our vision out of the boat, try to have an upwind plan and match it with the starting strategy, and the general idea above all it was 'take risk'.

## Downwind 

At the super light breeze moments, it was so important to keep the sails steady and try to move to the better side of the downwind. For 420s remember the crew should be forward a lot on very light conditions, forward the shrouds at the height of the mast step, the angle is high to inflate the spinakker and start building apparent wind with some speed, first is to get speed, second to try to get the angle down (closer to the gate, better VMG).

The last downwind on this course LR is a fight to be the inner boat on the gate, and on that way gybe and head first to the finish line, this needs to be prepared with enought time, far from the gate, working on the boat position to get to the mark with best chances to be the inner.

## Rules

When a boat should retire from a race? 

Our sport is a self ruled sport, everytime we notice we break a rule, we should use the 'Two Turns Penalty' to be able to keep on the competition and finish. On this Regatta, the Sailing Instructions modify the RRS (Racing Rules of Sailing), saying the Two Turns Penalty is replaced by One Turn Penalty (this is a bad idea I explain why below). 

::: warning In short
Any time we know we break a rule >>> then we take a penalty.
::: 

If we have an incident with other boat, it may end with a protest and at the protest room on land, the jury will decide if someone broke a rule and if there is any penalty, but if we understand we broke a rule, and maybe we noticed that after the opportunity to take the Penalty, we must retire from the race. To do that, we can approach on land, to the Race Office and just declare the intention. It saves time for everyone and it is **the right thing to do**.
If we break a rule and also taking the Penalty we still have advantage compared with no breaking rule path, we must retire from the race.

If there is a doubt about the incident or, who was breaking or not a rule, we should go to the protest procedure.

### Why One Turn Penalty?

As stated before, if we break a rule and we take the Penalty, we should not gain an advantage compared with not breaking the rule. If this happens, we must retire from the race. 

The Racing Rules of Sailing can be modified by the Sailing Instructions in some specific rules. BUT the change from Two Turns Penalty to One Turn Penalty usually is done on classes where the One Turn Penalty loose is enough to NOT gain advantage. [A Nacra 17 catamaran suffers very high taking one turn](https://youtu.be/4emdfoTvNKk?t=563), specially if she flyes with spinnaker and needs to put it down, and stop to zero speed and take a super slow turn (they call this the 'pontoon mode'), so on that case the One Turn Penalty has some logic, because has very high chances to punish enough.

But in other classes case, the One Turn Penalty has a **very high chance** to result in an advantage because those classes can take the turn very quick.
So, in generall, when this change is done, it is only for specific classes, not for everybody. 

This is a bad idea... as stated below by the [Racing Rules of Sailing](https://www.sailing.org/tools/documents/RRS20212024Finalwithbookmarks-[27255].pdf)

::: details 44 PENALTIES AT THE TIME OF AN INCIDENT

44.1 Taking a Penalty

A boat may take a Two-Turns Penalty when she may have broken one
or more rules of Part 2 in an incident while racing. She may take a
One-Turn Penalty when she may have broken rule 31. Alternatively,
the notice of race or sailing instructions may specify the use of the
Scoring Penalty or some other penalty, in which case the specified
penalty shall replace the One-Turn and the Two-Turns Penalty.

However,

44.2

(a)when a boat may have broken a rule of Part 2 and rule 31 in the
same incident she need not take the penalty for breaking rule 31;
(b)if the boat caused injury or serious damage **or, despite taking a
penalty, gained a significant advantage in the race or series by
her breach her penalty shall be to retire.**

:::