# Norcal - Richmond Yacht Club - sept 2022

## Racing course geometry 

Using small racing courses needs special attention to geometry, angles and distances in order to have a good strategy plan. 

In a small upwind leg, the start becomes more important, because the initial advantage you can gain at the starting line can make a big difference not having time to get speed gain, and not having distance or space to make a long term tactic. Boats who started first have a big chance to get to the windward mark first, and the very opposite with the lasts.

Also with a big starting line compared with the upwind lenght, the results in term of strategy is: if you start on one side of the line, the chances ending on that side of the course are very high.

::: warning So for small courses 
Start has high importance and must match with upwind tactics :rocket:
:::

## Light wind technique

Both were light air days, specially Saturday. The light wind technique is super difficult, specially upwind, and more with waves.
The main problem is the sync between [apparent wind](../glossary/README.md#apparent) changes and sails / weight settings.

There is a cycle of movement and adjustment very well explained in this next video [Body weight & Sheet sequences](https://www.youtube.com/watch?v=JlN-HzcwM1I)

Things become very complicated when there are also waves producing a second factor on [apparent wind](../glossary/README.md#apparent), like happened on Sunday.

### Super light wind

When the boat is almost stopped, and the sails are not being filled with air, and is really difficult to know where the wind is exactly comming from, you can say is a Super Light day... like Saturday... The wind speed can be under 2 knots, 3 knots. In general we need to keep a heeling angle going upwind so the sails keep still and with a shape, the main leech should be without tension, the boat should be still, with no crew movement at all, weights are in general moved forward, to keep trasom out of the water and the boat balanced on a straight path.

## Feeling feedback

The cycle keeps in a calm circular system but usually things change so we need to reset and adjust to start again the new cycle with the new state. This chained cycles are the really difficult thing to do, keeping transitions smooth and automatically done. On boats with more than one person is extremely difficult.

So we talked about the feedback we need to make the new reset for the new state, and the big problem is, we can not rely in our visuals (tell tails for example), because they result in a slow non updated signal... (also with a lack of precision). We must go with our feelings, in a sort of final 'prediction' sense, that is the only way the speed matches with the real changes. You can watch in previous video, the sailors are not watching, they are really focused 'feeling'.

## Wind patterns

The area we sailed was very close to a stepped shore... We can say there was wind up to windward but not reaching our area. There was a left big gust on Berkeley Circle / Alcatraz, and a right medium gust on the Tiburon / Angel Island channel and to the right. This two sources of wind pressure collided on our right making a dead zone that sometimes was dragged near to us making the right side very dangerous. This fight between two sources of different wind direction usually make that result: a dead zone.
The challenge on our course was to predict wich wind will win at any moment, and in general we can say on Saturday left had more time in than right, on Sunday it was balanced but at the end of the day it was more angled and unpredictable (maybe because the increased wind on the sources)

![Wind pattern Richmond](/sailing/richmond.png)

## Current

The current started filling in the first or first two races, that means aligned with the wind in angle and direction. Then when the water started going out of the bay, it reversed, and pointed against the wind. At that moment, fleet was getting over early at the starting line, and the result was a lot of general recalls. When this happens, there is no much to do as a competitor because, you need to be with the top fleet, on the first row at the start, of course everybody is trying to make a good start but the current pushing up makes the first line keep out of space, and heading down to try to get back on the line, ending generally in a fault over the leeward boat and a messy start. But there is no escape to this situation, the only advise should be try to keep at the favored side but out of the crowd. 
Conservative boats are more visible and is usuall they get disqualified because they are on sight and, people inside the crowd no because they are hidden.
The way to fight with this is having a big flag on the pin end, and better with a big vessel, so everybody can see the line end, a small mark does not work.

![Pin flag](/sailing/pin-flag.png)