# Rules quiz 2024

Small learning rules homework:

Pre requisites : please read Racing Rules of Sailing - Definitions, Part 2 Section A & B

1. Review these next cases and write down wich one is the Right of way boat and also rules and definitions applying.
2. Reproduce the case with this tool [Racing Rules tool](https://gd.games/ravecca/racing-rules-sailing) and review the logic between 'Right of way' and 'Limitations'.
3. Write down any confirmation or change about what you wrote before.
4. Next practice day bring this to discuss together!

## Case A

Thirty seconds before the starting signal, W was nearly stationary, her sails flapping. At least three hull lengths prior to becoming overlapped to leeward
of W, L hailed ‘Leeward boat’. W took no evasive action. Immediately after she became overlapped, L had to bear away to avoid contact with W;
meanwhile, W began to trim her sails and head up. L protested. 

![Case A](/sailing/c53.png)

## Case B

S and P, two keelboats about 24 feet (7 m) in length, approached each other on a windward leg, sailing at approximately the same speed in 12 to 15 knots
of wind and ‘minimal’ sea conditions. S was slightly ahead. When approximately three hull lengths away, S hailed ‘Starboard’ and did so again
at two hull lengths, but P did not respond or change course. At position 1 in the diagram both boats changed course at the same moment. S, fearing a
collision, luffed sharply intending to tack and thereby minimize damage or injury, and P bore away sharply. As soon as she saw P bear away, S
immediately bore away also. P, with her tiller turned as far to port as it would go, passed astern of S within two feet (0.6 m) of her. There was no contact.
S protested under rule 10.

![Case B](/sailing/c88.png)

## Case C

On a windward leg in winds of 18 knots, S and P approached each other on opposite tacks. P bore off to avoid S. S also bore off, and P continued
bearing off in order to pass astern of S. S also continued to bear off, heeling further to leeward as a result. There was contact between the masts and
rigging of the two boats and P’s mast was broken.

![Case C](/sailing/c92.png)