---
home: true
heroImage: /fj.jpg
# tagline: sailing drills
actionText: Latest debriefs →
actionLink: /debriefs/
footer: Made by Diego :)
---

::: warning New Rules homework!!
- [Rules homework Feb 4 cancelled practice](./debriefs/rulesquiz.md)
:::

::: tip 2023 Monterey
- [Monterey](./debriefs/monterey23.md)
:::

::: tip Training videos
- [420](./debriefs/420.md)
- [Optimist](./debriefs/optimist.md)
:::

::: details How this is made? 
:rocket: Some of you asked how this is made, if you are interested in programming or just curious...
This site is a "web-app" made with javascript, it works offline, you can test in 'airplane mode' after browsing some.

Made with following:

[Jamstack](https://jamstack.org/) Basic idea > Modern achitecture strategy for static sites.

[Vuepress](https://vuepress.vuejs.org/) Vuejs javascript framework, site generator for documentation.

[Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/) Deployed with Gitlab continuous integration / contiunous delivery (CI/CD) on Gitlab Pages

:::
