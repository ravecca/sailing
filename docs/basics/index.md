# Basics

[Body weight & Sheet sequences](https://www.youtube.com/watch?v=JlN-HzcwM1I)

## Three distance circles

This idea is about how we can focus to perceive what is happening on three different distances from our boat. 
The first circle refers to what is happenening on the fleet we are sailing with, it can be some feets around.
Second circle could be the course distance, the windward mark distance.
And a third circle refers to what we can see or perceive about a far away distance. We can observe another sailboats, and try to know if they have wind or not, what angle are they sailing, etc.

The perception on these three different distances gives us a better chance to **predict conditions** and therefore to have a **more accurate strategy**.

## Defending the space

Go to Configuration and set Auto Translate

[Video about how to defend your space on starts](https://youtu.be/Jqytj38nXJE?t=29)

## Acceleration

[Acceleration](https://youtu.be/hpYary87N_0?t=31)

## Tacking

[Tacking 1](https://youtu.be/vfKRRe9n9hk?t=14)

and a long about I420 but with 470 sailors on board

[420 to the max](https://youtu.be/ErcZk7nuzU4?t=144)