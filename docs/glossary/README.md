---
sidebar: auto
---

# Glossary

## Apparent wind

Is the result wind combined on true wind and boat's speed wind 
Apparent wind explanation [apparent wind](https://www.youtube.com/watch?v=CLfDqRgGUs0) 

## True wind
Is the wind at one fixed place. It's speed and direction is measured in knots and degrees. 

Speed. To measure speed we use an anemometer. One knot is one nautical mile per hour. (1,852 m/h) equals to 0.5 m/s (meters per second). That means 2 knts = 1 m/s, a more human measurable speed. 
![anemometer](/sailing/anemometer.jpg)

Direction. We use a compass. The circle is 360 degrees, with North being 0, South 180, East 90, West 270
The compass works with earth magnetic field and it has a different declination around the globe, that means a difference between real north and magnetic north. It can be insignificant to some degrees.

![coaches preferred compass](/sailing/compass.jpg "Coaches preferred compass")
::: warning Coaches preferred compass
It is used pointing to a line sight 
:::

![electronic compass](/sailing/electronic-compass.jpg)
::: warning Electronic compass
Used with classes like Nacra, 470, Lasers
:::