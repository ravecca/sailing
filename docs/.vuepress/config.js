module.exports = {
    title: 'Encinal YC Sailing',
    description: 'Notes, videos and resources for Encinal Yacht Club teams',
    base: '/sailing/',
    dest: 'public',
    ServiceWorker: true,
    themeConfig: {
      nav: [
        { text: 'Home', link: '/' },
        { text: 'Debriefs', link: '/debriefs/' },
        { text: 'Basics', link: '/basics/' },
        { text: 'Glossary', link: '/glossary/' },
        { text: 'Resources', link: '/resources/' },
      ],
      sidebar: 'auto'
    },
    
  }
